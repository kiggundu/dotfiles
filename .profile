# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi


## Abraham additions
alias tnix="sudo -u debian-tor nyx"

#Aliases
alias bex="bundle exec"
alias mapsvpn="sshuttle --dns -r vpn 10.20.0.0/13"
alias pex="python -m"
alias audiofy_file="festival --tts"

#Some Kitty aliases.. all begin with kt_
alias kticat="kitty +kitten icat"
alias ktd="kitty +kitten diff"

# ------------------------------------
# Docker alias and function
# ------------------------------------
alias sd="sudo docker"
# Get latest container ID
alias dl="docker ps -l -q"
# Get container process
alias dps="docker ps"
# Get process included stop container
alias dpa="docker ps -a"
# Get images
alias di="docker images"
# Get container IP
alias dip="docker inspect --format '{{ .NetworkSettings.IPAddress }}'"
# Run deamonized container, e.g., $dkd base /bin/echo hello
alias drd="docker run -d -P"
# Run interactive container, e.g., $dki base /bin/bash
alias dki="docker run -i -t -P"
# Execute interactive container, e.g., $dex base /bin/bash
alias dex="docker exec -i -t"
# Stop all containers
dstop() { docker stop $(docker ps -a -q); }
# Remove all containers
# Stop and Remove all containers
alias drmf='docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)'
# Remove all images
dri() { docker rmi $(docker images -q); }
# Remove all <none> images
drin() { docker rmi -f $(docker images --filter "dangling=true" -q); }
# Dockerfile build, e.g., $dbu tcnksm/test
dbu() { docker build -t=$1 .; }
# Show all alias related docker
dalias() { alias | grep 'docker' | sed "s/^\([^=]*\)=\(.*\)/\1 => \2/"| sed "s/['|\']//g" | sort; }

#Some keyboard shortcuts
dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/command "'/home/god/tools/speak_from_clipboard.sh'"
dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/binding "'<Primary><Shift><Alt>s'"
dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/name "'Speak text in the clipboard'"

#fabric aliases
alias pdhm="pandoc -f html -t markdown"
alias fab="fabric"
alias fabp="fabric -p"
alias fabsw="fabric --wipesession"
alias fabsp="fabric --printsession"
alias fabpp="xargs -I{} cat ~/.config/fabric/patterns/{}/system.md"
alias fabpl="fabric --listpatterns"
alias fabml="fabric --listmodels"
alias fabcl="fabric --listcontexts"
alias fabsl="fabric --listsessions"
alias fabsp="fabric --printsessions"
alias fabpi="tee temp_prompt.tmp | sha256sum | cut -d ' ' -f1 | xargs -I{} sh -c 'mkdir -p ~/.prompts/{} && mv temp_prompt.tmp ~/.prompts/{}/system.md && ln -sf ~/.prompts/{} ~/.config/fabric/patterns/{} && echo {}'" #| cat temp_prompt.tmp.... xargs -I{} tee prompts/{} 
alias fabpn='sed "s|^|$HOME\/.prompts\/|g" | sed "s|\ |\ $HOME\/.config\/fabric\/patterns\/|g" | xargs -t ln -sTf'
alias conv_add='tee -a conversation.txt'
#alias sys_say='sed "s/^/SYSTEM: /g" | conv_add'
#alias sys_say='awk \'{print "SYSTEM:\n"$1"\n"}\' | conv_add'
alias exp_say='cat <(echo "\nEXPERT:") - | conv_add'
alias sys_say='cat <(echo "\nSYSTEM:") - | conv_add'
alias usr_say='cat <(echo "\nUSER:") - | conv_add'
alias role_say='cat <(echo "\n$ROLE:") | conv_add'
alias USR='ROLE=USER && echo ""'
alias SYS='ROLE=SYSTEM && echo ""'
alias TCH='ROLE=TEACHER && echo ""'
alias EXP='ROLE=EXPERT && echo ""'
alias AST='ROLE=ASSISTANT && echo ""'

alias find_html_docs_by_content='grep -l "DOCTYPE html" **/* -d skip'
alias pdhm_path='xargs -I{} pandoc -f html -t markdown -t markdown_strict-raw_html --standalone -o {}.md {}'
#alias md_path_remove_links="xargs -I{} sed -i -e 's/([^()]*)//g' {}"
alias md_path_remove_links="xargs -I{} sed  -i -E 's/\[(.*)\]\(.*\)/\1/g' {}"

#TTS
#piper is a pipx installed python tool that accepts and speaks piped text
alias absay='piper --model en_US-lessac-medium --output-raw | aplay -r 22050 -f S16_LE -t raw -'
alias ai="echo "$(date +"%Y_%m_%d_%H_%M_%S")" | xargs -I{} sh -c 'arecord -d 5 -f cd /tmp/{}.wav && spchcat /tmp/{}.wav' | ollama run llama3.2 | absay"
alias ai10="echo "$(date +"%Y_%m_%d_%H_%M_%S")" | xargs -I{} sh -c 'arecord -d 10 -f cd /tmp/{}.wav && spchcat /tmp/{}.wav' | ollama run llama3.2 | absay"
alias aif="echo "$(date +"%Y_%m_%d_%H_%M_%S")" | xargs -I{} sh -c 'arecord -d 5 -f cd /tmp/{}.wav && spchcat /tmp/{}.wav' | fab | absay"
alias aifs="echo "$(date +"%Y_%m_%d_%H_%M_%S")" | xargs -I{} sh -c 'arecord -d 5 -f cd /tmp/{}.wav && spchcat /tmp/{}.wav' | fab --session "

alias txt_in="xargs -I{} sh -c 'cat {} | piper --model en_US-lessac-medium --output-raw | aplay -r 22050 -f S16_LE -t raw -'"
alias text_receive="inotifywait -q -m --format '%w%f'  ~/.prompts/text_user -e close_write | txt_in &"
alias fab_start='MINIONS=${HOME}/.prompts bento -c ~/.pipelines/main.yaml streams ~/.pipelines/streams/*.yaml'
alias fab_stop='killall -SIGINT bento'
alias ensure_fab_running='if [ $(pgrep bento | wc -l) -lt 1  ]; then fab_start; fi'
alias aiaud='ensure_fab_running | arecord -f cd ~/.prompts/voice_user/$(date +"%Y_%m_%d_%H_%M_%S").wav'


#Abe added kitty
export PATH="$PATH:$HOME/.local/kitty.app/bin"

#Abe added intel c compileer and dev tools to ath
export PATH="$PATH:$HOME/intel/sw_dev_tools/bin"

export PATH="$PATH:/Android/platform-tools:/Android/tools:/Android/bin"
export ANDROID_HOME="/Android"

#setup Kaldi
export KALDI_ROOT="$HOME/workspace/automated-voice-recognition/kaldi/kaldi"
export FSTROOT="$KALDI_ROOT/tools/openfst/"

export LC_ALL=en_US.UTF-8

export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"

#Ruby stuff
eval "$(rbenv init -)"

#JEnv JVM management
export PATH="$HOME/.jenv/bin:$PATH"
eval "$(jenv init -)"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

#set up perl
#source ~/perl5/perlbrew/etc/bashrc

#qmake stuff
export PATH="/usr/local/opt/qt/bin:$PATH"

#Required after openssl install otherwise mysql build will not be able to find the libraries
#failing with mysql2.bundle library not found
export LIBRARY_PATH=$LIBRARY_PATH:/usr/local/opt/openssl/lib/

#mvn stuff
export M2_HOME=/$HOME/.mvn/apache-maven-3.6.3
export PATH=$PATH:$M2_HOME/bin

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"
export PATH="$HOME/.jenv/bin:$PATH"
eval "$(jenv init -)"

#Go Configuration
export GOPATH=$HOME/workspace/go/go
export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin
export GOROOT=$HOME/workspace/go/go
export GOPATH=$HOME/workspace/go/work
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin
export PATH=$PATH:$HOME/.local/share/coursier/bin

#Ensure vim is my default editor
export EDITOR=vim

#Github CLI autocomplete
#autoload -U compinit
#compinit -i

#Abe additions
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init --path)"
eval "$(pyenv virtualenv-init -)"

#ensure opam, pyenv paths are available
#eval `opam config env`

export LD_LIBRARY_PATH=/usr/lib/wsl/lib:$LD_LIBRARY_PATH
#. "$HOME/ --no-tmux.cargo/env"
#GPT4All cli
alias gptai="python ~/gpt4all-cli/app.py repl --model ~/local-llm-models/default.gguf"
alias gptaiu="python ~/gpt4all-cli/app.py repl --model ~/local-llm-models/default_uncensored.gguf"
alias gptaix="python ~/gpt4all-cli/app.py repl --model ~/local-llm-models/qwen2-7b-instruct-q8_0.gguf"
alias gptui="~/lollms-webui/scripts/linux/linux_run.sh"

alias myip="curl  http://checkip.amazonaws.com/"
alias mytorip="torsocks curl  http://checkip.amazonaws.com/"

alias ddgl="BROWSER=links ddgr"
alias ddglx="BROWSER=lynx ddgr"
alias ddglt="BROWSER=links torsocks ddgr"
alias ddglxt="BROWSER=lynx torsocks ddgr"

# Override rails zsh plugin ovewriting sr alias
alias sr="surfraw"

export PATH="/home/god/.local/bin:$PATH"
export PATH="/home/god/workspace/flutter_dev/flutter/bin/:$PATH"
